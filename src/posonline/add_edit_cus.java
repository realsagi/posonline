package posonline;

import config.dbconnect;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import javax.swing.JOptionPane;

public class add_edit_cus extends javax.swing.JFrame {
    
    Connection conn = new dbconnect().getconnection();
    boolean checkedit_t_or_f = false;
    String tempnum,tempuser;
    
    public add_edit_cus() {
        initComponents();
        setIcon();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sexgroup = new javax.swing.ButtonGroup();
        usergroup = new javax.swing.ButtonGroup();
        namesex = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        pass1 = new javax.swing.JPasswordField();
        pass2 = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        lastname = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        firstname = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        num_em = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        man = new javax.swing.JRadioButton();
        woman = new javax.swing.JRadioButton();
        man1 = new javax.swing.JLabel();
        user = new javax.swing.JTextField();
        man2 = new javax.swing.JLabel();
        man3 = new javax.swing.JLabel();
        day = new javax.swing.JComboBox();
        month = new javax.swing.JComboBox();
        year = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        em_address = new javax.swing.JTextField();
        saveadd = new javax.swing.JButton();
        cleartext = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        friend_tel = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        other = new javax.swing.JTextArea();
        jLabel11 = new javax.swing.JLabel();
        admin = new javax.swing.JRadioButton();
        cashier = new javax.swing.JRadioButton();
        jLabel12 = new javax.swing.JLabel();
        friend_address = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        tel_em = new javax.swing.JTextField();
        edit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("เพิ่ม / แก้ไข พนักงาน");
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        namesex.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        namesex.setText("เพศ");

        jLabel2.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel2.setText("รหัสผ่าน");

        jLabel3.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel3.setText("นามสกุลพนักงาน");

        pass1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        pass2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        jLabel4.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel4.setText("รหัสผ่านอีกครั้ง");

        lastname.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N

        jLabel5.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel5.setText("ชื่อพนักงาน");

        firstname.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N

        jLabel6.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel6.setText("เลขประจำตัวประชาชน");

        num_em.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        num_em.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                num_emKeyReleased(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel7.setText("เบอร์โทร");

        sexgroup.add(man);
        man.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        man.setText("ชาย");

        sexgroup.add(woman);
        woman.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        woman.setText("หญิง");

        man1.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        man1.setText("-");

        user.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N

        man2.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        man2.setText("วัน-เดือน-ปี เกิด");

        man3.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        man3.setText("-");

        day.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        day.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "วัน", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        month.setFont(new java.awt.Font("BrowalliaUPC", 1, 24)); // NOI18N
        month.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "เดือน", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" }));

        year.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        year.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ปี", "2445", "2446", "2447", "2448", "2449", "2450", "2451", "2452", "2453", "2454", "2455", "2456", "2457", "2458", "2459", "2460", "2461", "2462", "2463", "2464", "2465", "2466", "2467", "2468", "2469", "2470", "2471", "2472", "2473", "2474", "2475", "2476", "2477", "2478", "2479", "2480", "2481", "2482", "2483", "2484", "2485", "2486", "2487", "2488", "2489", "2490", "2491", "2492", "2493", "2494", "2495", "2496", "2497", "2498", "2499", "2500", "2501", "2502", "2503", "2504", "2505", "2506", "2507", "2508", "2509", "2510", "2511", "2512", "2513", "2514", "2515", "2516", "2517", "2518", "2519", "2520", "2521", "2522", "2523", "2524", "2525", "2526", "2527", "2528", "2529", "2530", "2531", "2532", "2533", "2534", "2535", "2536", "2537", "2538", "2539", "2540", "2541", "2542", "2543", "2544", "2545", "2546", "2547", "2548", "2549", "2550", "2551", "2552", "2553", "2554", "2555", "2556", "2557", "2558", "2559", "2560", "2561", "2562", "2563", "2564", "2565", "2566", "2567", "2568", "2569", "2570", "2571", "2572", "2573", "2574", "2575", "2576", "2577", "2578", "2579", "2580", "2581", "2582", "2583", "2584", "2585", "2586", "2587", "2588", "2589", "2590", "2591", "2592", "2593", "2594", "2595", "2596", "2597", "2598", "2599", "2600" }));

        jLabel8.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel8.setText("ที่อยู่ปัจจุบัน พนักงาน");

        em_address.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N

        saveadd.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        saveadd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Floppy-Small-icon.png"))); // NOI18N
        saveadd.setText("บันทึก");
        saveadd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveaddMouseClicked(evt);
            }
        });

        cleartext.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        cleartext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-edit-clear-icon.png"))); // NOI18N
        cleartext.setText("ล้างข้อมูล");
        cleartext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cleartextMouseClicked(evt);
            }
        });

        exit.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/logout-icon.png"))); // NOI18N
        exit.setText("ออก");
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel9.setText("เบอร์โทรพนักงาน");

        friend_tel.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        friend_tel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                friend_telKeyReleased(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel10.setText("อื่นๆ");

        other.setColumns(20);
        other.setRows(5);
        jScrollPane1.setViewportView(other);

        jLabel11.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel11.setText("หน้าที่");

        usergroup.add(admin);
        admin.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        admin.setText("เจ้าของร้าน");

        usergroup.add(cashier);
        cashier.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        cashier.setText("พนักงานขาย");

        jLabel12.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel12.setText("ชื่อที่อยู่บุคคลที่ติดต่อได้");

        friend_address.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N

        jLabel13.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel13.setText("รหัสผู้ใช้");

        tel_em.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        tel_em.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tel_emKeyReleased(evt);
            }
        });

        edit.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        edit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pencil-icon.png"))); // NOI18N
        edit.setText("เลือกรายชื่อ");
        edit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(12, 12, 12)
                        .addComponent(firstname, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(lastname, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(em_address)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(friend_tel, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tel_em, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(user, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(pass1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(pass2, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(friend_address)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(namesex, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(man))
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel11))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGap(49, 49, 49)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(admin)
                                                .addGap(18, 18, 18)
                                                .addComponent(cashier)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addComponent(jScrollPane1)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(saveadd)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cleartext)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(edit)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(exit, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(woman)
                                        .addGap(79, 79, 79)
                                        .addComponent(man2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(day, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(man1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(man3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(year, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addComponent(num_em, javax.swing.GroupLayout.PREFERRED_SIZE, 615, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(42, 42, 42))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(firstname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lastname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(num_em, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(namesex, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(man)
                    .addComponent(woman)
                    .addComponent(man2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(day, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(man1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(month, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(man3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(year, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(em_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tel_em, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(friend_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(friend_tel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(user, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pass1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pass2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(admin)
                    .addComponent(cashier))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(saveadd)
                        .addComponent(cleartext, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(exit, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(edit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveaddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveaddMouseClicked
        try {
            boolean checkintforid = checkint(num_em.getText());
            boolean checktel_em = checkint(tel_em.getText());
            boolean checktel_friend = checkint(friend_tel.getText());
            if("".equals(firstname.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอกชื่อพนักงานด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                firstname.requestFocus();
            }
            else if("".equals(lastname.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอกนามสกุลพนักงานด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                lastname.requestFocus();
            }
            else if("".equals(num_em.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอกเลขประจำตัวประชาชนด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                num_em.requestFocus();
            }
            else if(num_em.getText().length() < 13){
                JOptionPane.showMessageDialog(this,"กรุณากรอกเลขประจำตัวให้ครบ 13 หลักด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                num_em.requestFocus();
                num_em.selectAll();
            }
            else if(num_em.getText().length() > 13){
                JOptionPane.showMessageDialog(this,"กรอกเลขบัตรประชาชนแค่ 13 หลักเท่านั้นค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                num_em.requestFocus();
                num_em.selectAll();
            }
            else if(checkintforid == false){
                JOptionPane.showMessageDialog(this,"เลขบัตรประชาชน ต้องเป็นตัวเลขเท่านั้นค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                num_em.requestFocus();
                num_em.selectAll();
            }
            else if(man.isSelected() == false && woman.isSelected() == false){
                JOptionPane.showMessageDialog(this,"กรุณาระบุเพศด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
            }
            else if(day.getSelectedIndex() == 0 || month.getSelectedIndex() == 0 || year.getSelectedIndex() == 0){
                JOptionPane.showMessageDialog(this,"กรุณาระบุ วัน-เดือน-ปี เกิดให้ครบถ้วน ด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
            }
            else if("".equals(em_address.getText())){
                JOptionPane.showMessageDialog(this,"กรุณาระบุที่อยู่ปัจจุบัน ของพนักงาน ด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                em_address.requestFocus();
            }
            else if("".equals(tel_em.getText())){
                JOptionPane.showMessageDialog(this,"กรุณาระบุเบอร์โทรของพนังานด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                tel_em.requestFocus();
                tel_em.selectAll();
            }
            else if(tel_em.getText().length()<10){
                JOptionPane.showMessageDialog(this,"กรุณากรอกเบอร์โทรให้ครบ 10 หลักด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                tel_em.requestFocus();
                tel_em.selectAll();
            }
            else if(tel_em.getText().length()>10){
                JOptionPane.showMessageDialog(this,"กรุณารกรอกเบอร์โทรแค่ 10 หลักเท่านั้นค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                tel_em.requestFocus();
                tel_em.selectAll();
            }
            else if(checktel_em == false){
                JOptionPane.showMessageDialog(this,"กรุณากรอกเบอร์โทรเป็นตัวเลขเท่านั้นค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                tel_em.requestFocus();
                tel_em.selectAll();
            }
            else if("".equals(friend_address.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอกที่อยู่ ของ เพื่อนพนักงาน ที่สามารถติดต่อได้ ค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                friend_address.requestFocus();
            }
            else if("".equals(friend_tel.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอก เบอร์โทร เพื่อน พนักงาน ค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                friend_tel.requestFocus();
            }
            else if(friend_tel.getText().length()<10){
                JOptionPane.showMessageDialog(this,"กรุณากรอกเบอร์โทรเพื่อนพนักงานให้ครบ 10 หลักค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                friend_tel.requestFocus();
                friend_tel.selectAll();
            }
            else if(friend_tel.getText().length()>10){
                JOptionPane.showMessageDialog(this,"กรุณากรอกเบอร์โทรเพื่อนพนักงานแค่ 10 หลักเท่านั้นค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                friend_tel.requestFocus();
                friend_tel.selectAll();
            }
            else if(checktel_friend == false){
                JOptionPane.showMessageDialog(this,"เบอร์โทรเพื่อนพนักงาน ต้องเป็นตัวเลขเท่านั้นค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                friend_tel.requestFocus();
                friend_tel.selectAll();
            }
            else if("".equals(user.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอกชื่อผู้ใช้ด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                user.requestFocus();
            }
            else if("".equals(pass1.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอกรหัสผ่านด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                pass1.requestFocus();
            }
            else if(!pass1.getText().equals(pass2.getText())){
                JOptionPane.showMessageDialog(this,"รหัสผ่านไม่ตรงกันค่ะ กรุณากรอกใหม่ ค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
                pass1.setText("");
                pass2.setText("");
                pass1.requestFocus();
            }
            else if(admin.isSelected() == false && cashier.isSelected() == false){
                JOptionPane.showMessageDialog(this,"กรุณาเลือกหน้าที่ของพนักงานด้วยค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
            }
            else if(checkedit_t_or_f == true){
                updateem();
            }
            else{
                addnewem();
            }
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_saveaddMouseClicked

    public boolean checkint(String s){
        try{
            Long.parseLong(s);
        }
        catch(NumberFormatException e){
            return false;
        }
        return true;
    }    
    private void cleartextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cleartextMouseClicked
        cleartext();
    }//GEN-LAST:event_cleartextMouseClicked

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        this.dispose();
    }//GEN-LAST:event_exitMouseClicked

    private void editMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editMouseClicked
        edit_em editcus = new edit_em();
        editcus.show();
        this.dispose();
    }//GEN-LAST:event_editMouseClicked

    private void num_emKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_num_emKeyReleased
        if(num_em.getText().length() >=14){
            num_em.setText(num_em.getText().substring(0, 13));
        }
    }//GEN-LAST:event_num_emKeyReleased

    private void tel_emKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tel_emKeyReleased
        if(tel_em.getText().length() >= 11){
            tel_em.setText("0"+tel_em.getText().substring(1,10));
        }
        else if(tel_em.getText().length() == 10){
            tel_em.setText("0"+tel_em.getText().substring(1,10));
        }
    }//GEN-LAST:event_tel_emKeyReleased

    private void friend_telKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_friend_telKeyReleased
        if(friend_tel.getText().length() >= 11){
            friend_tel.setText("0"+friend_tel.getText().substring(1,10));
        }
        else if(friend_tel.getText().length() == 10){
            friend_tel.setText("0"+friend_tel.getText().substring(1,10));
        }
    }//GEN-LAST:event_friend_telKeyReleased

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new add_edit_cus().setVisible(true);                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton admin;
    private javax.swing.JRadioButton cashier;
    private javax.swing.JButton cleartext;
    private javax.swing.JComboBox day;
    private javax.swing.JButton edit;
    private javax.swing.JTextField em_address;
    private javax.swing.JButton exit;
    private javax.swing.JTextField firstname;
    private javax.swing.JTextField friend_address;
    private javax.swing.JTextField friend_tel;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField lastname;
    private javax.swing.JRadioButton man;
    private javax.swing.JLabel man1;
    private javax.swing.JLabel man2;
    private javax.swing.JLabel man3;
    private javax.swing.JComboBox month;
    private javax.swing.JLabel namesex;
    private javax.swing.JTextField num_em;
    private javax.swing.JTextArea other;
    private javax.swing.JPasswordField pass1;
    private javax.swing.JPasswordField pass2;
    private javax.swing.JButton saveadd;
    private javax.swing.ButtonGroup sexgroup;
    private javax.swing.JTextField tel_em;
    private javax.swing.JTextField user;
    private javax.swing.ButtonGroup usergroup;
    private javax.swing.JRadioButton woman;
    private javax.swing.JComboBox year;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
    
    public void cleartext(){
        saveadd.setText("บันทึก");
        firstname.setText("");
        lastname.setText("");
        num_em.setText("");
        sexgroup.clearSelection();
        day.setSelectedIndex(0);
        month.setSelectedIndex(0);
        year.setSelectedIndex(0);
        em_address.setText("");
        tel_em.setText("");
        friend_address.setText("");
        friend_tel.setText("");
        user.setText("");
        pass1.setText("");
        pass2.setText("");
        usergroup.clearSelection();
        other.setText("");
    }
    
    public void updateem(){
        try {
            String newem1 = "UPDATE employee SET fname =  ?," +
                        "lname =  ?," +
                        "sex =  ?," +
                        "birthday =  ?," +
                        "em_address =  ?," +
                        "friend_address =  ?," +
                        "tel_em =  ?," +
                        "tel_friend =  ?," +
                        "other =  ?," +
                        "num_em =  ?," +
                        "user = ? WHERE employee.user = ? AND CONCAT(employee.num_em) = ? AND CONVERT(employee.user USING utf8 ) = ?;";       
            PreparedStatement sentsql = conn.prepareStatement(newem1);
            sentsql.setString(1, firstname.getText());
            sentsql.setString(2, lastname.getText());
            if(man.isSelected()){sentsql.setString(3, man.getText());}
            else if(woman.isSelected()){sentsql.setString(3, woman.getText());}
            sentsql.setString(4, Integer.parseInt(year.getSelectedItem().toString())-543+"-"+month.getSelectedIndex()+"-"+day.getSelectedIndex());
            sentsql.setString(5, em_address.getText());
            sentsql.setString(6, friend_address.getText());
            sentsql.setInt(7, Integer.parseInt(tel_em.getText()));
            sentsql.setInt(8, Integer.parseInt(friend_tel.getText()));
            sentsql.setString(9, other.getText());
            sentsql.setDouble(10, Double.parseDouble(num_em.getText()));
            sentsql.setString(11, user.getText());
            sentsql.setString(12, user.getText());
            sentsql.setString(13, tempnum);
            sentsql.setString(14, tempuser);
            String newem2 = "UPDATE user SET user = ?, pass = ?, usergroup = ? WHERE user.user = ? ;";
            PreparedStatement sentsql2 = conn.prepareStatement(newem2);
            sentsql2.setString(1,user.getText());
            sentsql2.setString(2,pass1.getText());
            if(admin.isSelected()){sentsql2.setString(3,"admin");}
            else if(cashier.isSelected()){sentsql2.setString(3,"cashier");}
            sentsql2.setString(4, tempuser);
            
            if(sentsql2.executeUpdate() != -1 && sentsql.executeUpdate() != -1){
                JOptionPane.showMessageDialog(this,"แก้ไขข้อมูลเรียบร้อยค่ะ","บันทึกข้อมูล",JOptionPane.PLAIN_MESSAGE);
                cleartext();
                checkedit_t_or_f = false;
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void addnewem() throws ParseException{
        try {
            String newem = "INSERT INTO employee(" +
                    "id_em,fname,lname,sex,birthday,em_address,friend_address,tel_em,tel_friend,other,num_em,user)"+
                    "VALUES(null,?,?,?,?,?,?,?,?,?,?,?);";
            PreparedStatement sentsql = conn.prepareStatement(newem);
            sentsql.setString(1, firstname.getText());
            sentsql.setString(2, lastname.getText());
            if(man.isSelected()){sentsql.setString(3, man.getText());}
            else if(woman.isSelected()){sentsql.setString(3, woman.getText());}
            sentsql.setString(4, Integer.parseInt(year.getSelectedItem().toString())-543+"-"+month.getSelectedIndex()+"-"+day.getSelectedIndex());
            sentsql.setString(5, em_address.getText());
            sentsql.setString(6, friend_address.getText());
            sentsql.setInt(7, Integer.parseInt(tel_em.getText()));
            sentsql.setInt(8, Integer.parseInt(friend_tel.getText()));
            sentsql.setString(9, other.getText());
            sentsql.setDouble(10, Double.parseDouble(num_em.getText()));
            sentsql.setString(11, user.getText());
            
            String newusergroup = "INSERT INTO user(user,pass,usergroup) VALUES(?,?,?);";
            PreparedStatement sentsql2 = conn.prepareStatement(newusergroup);
            sentsql2.setString(1,user.getText());
            sentsql2.setString(2,pass1.getText());
            if(admin.isSelected()){sentsql2.setString(3,"admin");}
            else if(cashier.isSelected()){sentsql2.setString(3,"cashier");}
            
            if(sentsql.executeUpdate() != -1 && sentsql2.executeUpdate() != -1){
                JOptionPane.showMessageDialog(this,"บันทึกข้อมูลเสร็จสิ้น","บันทึกข้อมูล",JOptionPane.PLAIN_MESSAGE);
                cleartext();
            }
            
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
    public void editemployee(String s){
        String editsql = "SELECT employee.fname,employee.lname,employee.sex,employee.birthday,employee.em_address,employee.friend_address,employee.tel_em,employee.tel_friend,employee.other,employee.num_em,user.user,user.pass,user.usergroup FROM  employee INNER JOIN user ON employee.user = user.user";
        try{
            ResultSet out = conn.createStatement().executeQuery(editsql);
            saveadd.setText("แก้ไข");
            while(out.next()){
                if(s.equals(out.getString("user"))){
                    firstname.setText(out.getString("fname"));
                    lastname.setText(out.getString("lname"));
                    num_em.setText(out.getString("num_em"));
                    if("ชาย".equals(out.getString("sex"))){man.setSelected(true);}
                    else if("หญิง".equals(out.getString("sex"))){woman.setSelected(true);}
                    day.setSelectedIndex(Integer.parseInt(out.getString("birthday").substring(out.getString("birthday").lastIndexOf("-")+1, out.getString("birthday").length())));
                    month.setSelectedIndex(Integer.parseInt(out.getString("birthday").substring(out.getString("birthday").indexOf("-")+1, out.getString("birthday").lastIndexOf("-"))));
                    int datayear = (Integer.parseInt(out.getString("birthday").substring(0, out.getString("birthday").indexOf("-")))+543);
                    year.setSelectedItem(String.valueOf(datayear));
                    em_address.setText(out.getString("em_address"));
                    tel_em.setText("0"+out.getString("tel_em"));
                    friend_address.setText(out.getString("friend_address"));
                    friend_tel.setText("0"+out.getString("tel_friend"));
                    user.setText(out.getString("user"));
                    pass1.setText(out.getString("pass"));
                    pass2.setText(out.getString("pass"));
                    if("admin".equals(out.getString("usergroup"))){admin.setSelected(true);}
                    else if("cashier".equals(out.getString("usergroup"))){cashier.setSelected(true);}
                    other.setText(out.getString("other"));
                    tempnum = out.getString("num_em");
                    tempuser = out.getString("user");
                    break;
                }
            }
            checkedit_t_or_f = true;
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }

    }
}
