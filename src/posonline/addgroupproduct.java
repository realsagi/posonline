package posonline;

import config.dbconnect;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public final class addgroupproduct extends javax.swing.JFrame {
    
    Connection conn = new dbconnect().getconnection();
    DefaultTableModel modellistgroup;
    String groupname;

    public addgroupproduct() {
        initComponents();
        setIcon();
        searchdata();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        exit = new javax.swing.JButton();
        namegroup = new javax.swing.JLabel();
        txtnamegroupprod = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        listgroup = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("เพิ่ม/ลด หมวดสินค้า");
        setBounds(new java.awt.Rectangle(500, 100, 540, 500));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setMaximizedBounds(new java.awt.Rectangle(500, 100, 540, 500));
        setMaximumSize(new java.awt.Dimension(540, 500));
        setMinimumSize(new java.awt.Dimension(540, 500));
        setName("addgroupproduct"); // NOI18N
        setPreferredSize(new java.awt.Dimension(540, 500));

        exit.setFont(new java.awt.Font("JasmineUPC", 1, 18)); // NOI18N
        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Apps-session-logout-icon.png"))); // NOI18N
        exit.setText("ออก");
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });

        namegroup.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        namegroup.setText("ชื่อหมวดสินค้า");

        txtnamegroupprod.setFont(new java.awt.Font("JasmineUPC", 1, 36)); // NOI18N
        txtnamegroupprod.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnamegroupprodKeyReleased(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("JasmineUPC", 0, 18)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Device-Floppy-icon.png"))); // NOI18N
        jButton1.setText("บันทึก");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-edit-clear-icon.png"))); // NOI18N
        jButton2.setText("ลบ");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        listgroup.setAutoCreateRowSorter(true);
        listgroup.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        listgroup.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        listgroup.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ลำดับ", "ชื่อหมวดสินค้า"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        listgroup.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        listgroup.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        listgroup.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        listgroup.setDropMode(javax.swing.DropMode.ON);
        listgroup.setRowHeight(26);
        listgroup.setRowMargin(3);
        listgroup.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listgroup.setSurrendersFocusOnKeystroke(true);
        listgroup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listgroupMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(listgroup);
        if (listgroup.getColumnModel().getColumnCount() > 0) {
            listgroup.getColumnModel().getColumn(0).setMinWidth(70);
            listgroup.getColumnModel().getColumn(0).setPreferredWidth(20);
            listgroup.getColumnModel().getColumn(0).setMaxWidth(70);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(namegroup)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtnamegroupprod, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(exit)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(exit, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(namegroup, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtnamegroupprod, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton2))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        this.dispose();
    }//GEN-LAST:event_exitMouseClicked

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        insertdata();
    }//GEN-LAST:event_jButton1MouseClicked

    private void listgroupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listgroupMouseClicked
        int index = listgroup.getSelectedRow();
        this.groupname = (String) listgroup.getValueAt(index, 1);
    }//GEN-LAST:event_listgroupMouseClicked

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        try{
            String sql = "DELETE FROM namegroupprod WHERE namegroupprod.name = '"+this.groupname+"'";
            if(conn.createStatement().executeUpdate(sql) != -1){
                searchdata();
            }
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton2MouseClicked

    private void txtnamegroupprodKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnamegroupprodKeyReleased
         if(evt.getKeyCode() == 10){
            insertdata();
         }
    }//GEN-LAST:event_txtnamegroupprodKeyReleased

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new addgroupproduct().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton exit;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable listgroup;
    private javax.swing.JLabel namegroup;
    private javax.swing.JTextField txtnamegroupprod;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
    
    public void searchdata(){
        modellistgroup = (DefaultTableModel) listgroup.getModel();
        try{
            int totalrow = listgroup.getRowCount()-1;
            while(totalrow > -1){
                modellistgroup.removeRow(totalrow);
                totalrow --;
            }
            String sql = "SELECT * FROM namegroupprod";
            ResultSet rs = conn.createStatement().executeQuery(sql);
            int row = 0;
            while(rs.next()){
                modellistgroup.addRow(new Object[0]);
                modellistgroup.setValueAt(row+1, row, 0);
                modellistgroup.setValueAt(rs.getString("name"), row, 1);
                row++;
            }
            listgroup.setModel(modellistgroup);
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
    public void insertdata(){
        try{
            String sql = "INSERT INTO namegroupprod (id, name) VALUES (NULL, ?);";
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, txtnamegroupprod.getText());
            if(pre.executeUpdate() != -1){
                searchdata();
            }
            txtnamegroupprod.setText("");
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
}
