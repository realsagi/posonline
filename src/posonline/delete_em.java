package posonline;

import config.dbconnect;
import java.awt.Component;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class delete_em extends javax.swing.JFrame {
    
    Connection conn = new dbconnect().getconnection();
    DefaultTableModel modellistgroup;
    String username = "";

    public delete_em() {
        initComponents();
        setIcon();
        printem();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        exit = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        listallem = new javax.swing.JTable();
        delete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("ลดข้อมูลพนักงาน");
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        exit.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-exit-icon.png"))); // NOI18N
        exit.setText("ออก");
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });

        listallem.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        listallem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ลำดับ", "ชือ - นามสกุล", "เบอร์โทร", "รหัสพนักงาน", "หน้าที่"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        listallem.setRowHeight(30);
        listallem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listallemMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(listallem);
        if (listallem.getColumnModel().getColumnCount() > 0) {
            listallem.getColumnModel().getColumn(0).setMinWidth(5);
            listallem.getColumnModel().getColumn(0).setPreferredWidth(5);
            listallem.getColumnModel().getColumn(1).setMinWidth(100);
            listallem.getColumnModel().getColumn(1).setPreferredWidth(100);
            listallem.getColumnModel().getColumn(2).setMinWidth(70);
            listallem.getColumnModel().getColumn(2).setPreferredWidth(70);
            listallem.getColumnModel().getColumn(3).setMinWidth(50);
            listallem.getColumnModel().getColumn(3).setPreferredWidth(50);
            listallem.getColumnModel().getColumn(4).setMinWidth(50);
            listallem.getColumnModel().getColumn(4).setPreferredWidth(50);
        }

        delete.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-edit-clear-icon.png"))); // NOI18N
        delete.setText("ลบ");
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deleteMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(delete, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(exit)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(exit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(delete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        this.dispose();
    }//GEN-LAST:event_exitMouseClicked

    private void listallemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listallemMouseClicked
        int index = listallem.getSelectedRow();
        this.username = (String) listallem.getValueAt(index, 3);
    }//GEN-LAST:event_listallemMouseClicked

    private void deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteMouseClicked
        if("".equals(username)){
            JOptionPane.showMessageDialog(this,"กรุณาเลือกรายการก่อนค่ะ","เลือกรายการ",JOptionPane.PLAIN_MESSAGE);
        }
        else{
            deleteemployee();
        }
    }//GEN-LAST:event_deleteMouseClicked

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new delete_em().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton delete;
    private javax.swing.JButton exit;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable listallem;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
    
    public void deleteemployee(){
        try {
            String sqldelete = "DELETE FROM employee WHERE employee.user = ?";
            String sqldelete2 = "DELETE FROM user WHERE user.user = ?";
            PreparedStatement predelete = conn.prepareStatement(sqldelete);
            PreparedStatement predelete2 = conn.prepareStatement(sqldelete2);
            Object[] options = {"ใช่",
                    "ไม่"};
            Component frame = null;
            int n = JOptionPane.showOptionDialog(frame,
                "คุณแน่ใจ หรือ ไม่ ที่จะลบข้อมูลนี้ ออกจากฐานข้อมูล",
                "คุณมั่นใจหรือไม่",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,     //do not use a custom Icon
                options,  //the titles of buttons
                options[0]); //default button title
            if(n == 0){
                predelete.setString(1, username);
                predelete2.setString(1, username);
                if(predelete.executeUpdate() != -1 && predelete2.executeUpdate() != -1){
                    JOptionPane.showMessageDialog(this,"ลบข้อมูลเรียบร้อยแล้วค่ะ","ลบข้อมูล",JOptionPane.PLAIN_MESSAGE);
                    printem();
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public final void printem(){
        modellistgroup = (DefaultTableModel) listallem.getModel();
        try{
            int totalrow = listallem.getRowCount()-1;
            while(totalrow > -1){
                modellistgroup.removeRow(totalrow);
                totalrow --;
            }
            String listallcus = "SELECT employee.fname,employee.tel_em,employee.lname,employee.user,user.usergroup FROM  employee INNER JOIN user ON employee.user = user.user";
            ResultSet rs = conn.createStatement().executeQuery(listallcus);
            int row = 0;
            while(rs.next()){
                modellistgroup.addRow(new Object[0]);
                modellistgroup.setValueAt(row+1, row, 0);
                modellistgroup.setValueAt(rs.getString("fname")+"  "+rs.getString("lname"), row, 1);
                modellistgroup.setValueAt("0"+rs.getString("tel_em"), row, 2);
                modellistgroup.setValueAt(rs.getString("user"), row, 3);
                modellistgroup.setValueAt(rs.getString("usergroup"), row, 4);
                row++;
            }
            listallem.setModel(modellistgroup);
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
}
