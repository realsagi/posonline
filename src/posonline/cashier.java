package posonline;

import config.dbconnect;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.table.DefaultTableModel;

public class cashier extends javax.swing.JFrame {
    
    Connection conn = new dbconnect().getconnection();
    DefaultTableModel modellistgroup;
    ArrayList<String> arrayid = new ArrayList<>();
    ArrayList<String> arraynameprod = new ArrayList<>();
    ArrayList<String> arraynameunit = new ArrayList<>();
    ArrayList<Integer> arraycount = new ArrayList<>();
    ArrayList<Integer> arrayrow = new ArrayList<>();
    ArrayList<Double> arraysell = new ArrayList<>();
    ArrayList<Double> arraysum = new ArrayList<>();
    
    public void setnameem(String s){
        name_em.setText(s);
        System.out.println("รับ "+s);
    }

    public cashier() {
        initComponents();
        setIcon();
        defalsesetpage();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        vatgroup = new javax.swing.ButtonGroup();
        pricesum = new javax.swing.JLabel();
        name_em = new javax.swing.JLabel();
        scan = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        vat = new javax.swing.JRadioButton();
        novat = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        count = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        selltable = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        discount = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        sumallprice = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("ขายสินค้า");
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(255, 255, 51));
        setBounds(new java.awt.Rectangle(0, 0, 1280, 768));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setMaximizedBounds(new java.awt.Rectangle(0, 0, 1366, 768));
        setMaximumSize(new java.awt.Dimension(1366, 768));
        setMinimumSize(new java.awt.Dimension(1280, 768));
        setName("cashier"); // NOI18N
        setType(java.awt.Window.Type.UTILITY);

        pricesum.setBackground(new java.awt.Color(0, 0, 0));
        pricesum.setFont(new java.awt.Font("AngsanaUPC", 1, 120)); // NOI18N
        pricesum.setForeground(new java.awt.Color(255, 102, 51));
        pricesum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        pricesum.setLabelFor(pricesum);
        pricesum.setText("0.00");
        pricesum.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 0, 0)));
        pricesum.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        pricesum.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        pricesum.setOpaque(true);

        name_em.setFont(new java.awt.Font("JasmineUPC", 1, 40)); // NOI18N

        scan.setFont(new java.awt.Font("AngsanaUPC", 1, 60)); // NOI18N
        scan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                scanMouseClicked(evt);
            }
        });
        scan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                scanKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("JasmineUPC", 1, 40)); // NOI18N
        jLabel2.setText("รหัสสินค้า");

        jLabel3.setFont(new java.awt.Font("JasmineUPC", 1, 40)); // NOI18N
        jLabel3.setText("ชื่อผู้ขาย");

        jLabel1.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel1.setText("ภาษี");

        vatgroup.add(vat);
        vat.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        vat.setText("คิดภาษี");

        vatgroup.add(novat);
        novat.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        novat.setText("ไม่คิดภาษี");

        jButton1.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jButton1.setText("พิมพ์ใบเสร็จ");

        exit.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-exit-icon.png"))); // NOI18N
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jButton4.setText("พิมพ์ใบกำกับภาษี");

        jButton2.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jButton2.setText("จบการขาย");

        jLabel4.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel4.setText("ส่วนลด");

        count.setFont(new java.awt.Font("BrowalliaUPC", 1, 24)); // NOI18N
        count.setText("1");
        count.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                countMouseClicked(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel5.setText("บาท");

        selltable.setAutoCreateRowSorter(true);
        selltable.setBorder(new javax.swing.border.MatteBorder(null));
        selltable.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        selltable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ลำดับ", "รหัสสินค้า", "ชื่อสินค้า", "ราคาต่อหน่วย", "หน่วย", "จำนวน", "รวม"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        selltable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        selltable.setGridColor(new java.awt.Color(153, 153, 153));
        selltable.setRowHeight(20);
        selltable.getTableHeader().setReorderingAllowed(false);
        selltable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                selltableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(selltable);
        if (selltable.getColumnModel().getColumnCount() > 0) {
            selltable.getColumnModel().getColumn(0).setPreferredWidth(1);
            selltable.getColumnModel().getColumn(1).setPreferredWidth(100);
            selltable.getColumnModel().getColumn(2).setPreferredWidth(300);
            selltable.getColumnModel().getColumn(3).setResizable(false);
            selltable.getColumnModel().getColumn(3).setPreferredWidth(50);
            selltable.getColumnModel().getColumn(4).setPreferredWidth(50);
            selltable.getColumnModel().getColumn(5).setResizable(false);
            selltable.getColumnModel().getColumn(5).setPreferredWidth(50);
            selltable.getColumnModel().getColumn(6).setResizable(false);
            selltable.getColumnModel().getColumn(6).setPreferredWidth(50);
        }

        jLabel6.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel6.setText("จำนวน");

        discount.setFont(new java.awt.Font("BrowalliaUPC", 1, 24)); // NOI18N
        discount.setText("0.00");
        discount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                discountMouseClicked(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("JasmineUPC", 1, 36)); // NOI18N
        jLabel7.setText("ราคารวมทั้งสิ้น");

        jLabel8.setFont(new java.awt.Font("JasmineUPC", 1, 36)); // NOI18N
        jLabel8.setText("ภาษี 7%");

        jLabel9.setFont(new java.awt.Font("JasmineUPC", 1, 36)); // NOI18N
        jLabel9.setText("ราคาไม่รวมภาษี");

        jTextField1.setBackground(new java.awt.Color(0, 0, 0));
        jTextField1.setFont(new java.awt.Font("AngsanaUPC", 1, 36)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(255, 0, 0));
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.setText("0.00");
        jTextField1.setCaretColor(new java.awt.Color(255, 51, 51));
        jTextField1.setDisabledTextColor(new java.awt.Color(255, 0, 0));
        jTextField1.setEnabled(false);

        jTextField2.setBackground(new java.awt.Color(0, 0, 0));
        jTextField2.setFont(new java.awt.Font("AngsanaUPC", 1, 36)); // NOI18N
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField2.setText("0.00");
        jTextField2.setDisabledTextColor(new java.awt.Color(255, 0, 0));
        jTextField2.setEnabled(false);

        sumallprice.setBackground(new java.awt.Color(0, 0, 0));
        sumallprice.setFont(new java.awt.Font("AngsanaUPC", 1, 36)); // NOI18N
        sumallprice.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        sumallprice.setText("0.00");
        sumallprice.setDisabledTextColor(new java.awt.Color(255, 0, 0));
        sumallprice.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(discount, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5)
                                .addGap(37, 37, 37)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(count, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(vat)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(novat)
                                .addGap(0, 186, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(name_em, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(scan, javax.swing.GroupLayout.Alignment.TRAILING))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pricesum, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jButton2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton4))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(exit, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                            .addComponent(sumallprice))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(name_em, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scan, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(novat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(vat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(count, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(discount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pricesum, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(sumallprice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(exit, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(93, 93, 93))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        this.dispose();
    }//GEN-LAST:event_exitMouseClicked

    private void countMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_countMouseClicked
        count.selectAll();
    }//GEN-LAST:event_countMouseClicked

    private void scanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scanMouseClicked
        scan.selectAll();
    }//GEN-LAST:event_scanMouseClicked

    private void discountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_discountMouseClicked
        discount.selectAll();
    }//GEN-LAST:event_discountMouseClicked
    
    private void scanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_scanKeyReleased
        try {
            boolean insertboolean = true;
            int checksame = 0;
            while(checksame < arrayid.size()){
                if(scan.getText().equals(arrayid.get(checksame))){
                    int sumcount = arraycount.get(checksame)+Integer.parseInt(count.getText());
                    arraycount.remove(checksame);
                    arraycount.add(checksame, sumcount);
                    double sumsell = arraysell.get(checksame)*arraycount.get(checksame);
                    arraysum.remove(checksame);
                    arraysum.add(checksame,sumsell);
                    insertboolean = false;
                    scan.setText("");
                    count.setText("1");
                    scan.requestFocus();
                    break;
                }
                else{
                    insertboolean = true;
                }
                checksame ++;
            }
            if(insertboolean == true){
                insertarray();
            }
            showtablelist();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_scanKeyReleased

    public void showtablelist(){
        int totalrow = selltable.getRowCount()-1;
        int idx = 0;
        double sumall = 0;
        while(totalrow > -1){
            modellistgroup.removeRow(totalrow);
            totalrow --;
        }
        if(totalrow == -1){
            pricesum.setText("0.00");
            sumallprice.setText("0.00");
            scan.setText("");
            count.setText("1");
            scan.requestFocus();
        }
        while(idx < arrayid.size()){
            modellistgroup.addRow(new Object[0]);
            modellistgroup.setValueAt(arrayrow.get(idx), idx, 0);
            modellistgroup.setValueAt(arrayid.get(idx), idx, 1);
            modellistgroup.setValueAt(arraynameprod.get(idx), idx, 2);
            modellistgroup.setValueAt(arraysell.get(idx), idx, 3);
            modellistgroup.setValueAt(arraynameunit.get(idx), idx, 4);
            modellistgroup.setValueAt(arraycount.get(idx), idx, 5);
            modellistgroup.setValueAt(arraysum.get(idx), idx, 6);
            sumall = sumall + arraysum.get(idx);
            String result = String.format("%,.2f", sumall);
            pricesum.setText(result);
            sumallprice.setText(pricesum.getText());
            idx++;
        }
    }
    
    private void selltableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_selltableMouseClicked
        if(selltable.getSelectedRow() != -1 && evt.getModifiers() == 4){
            final JPopupMenu popup = new JPopupMenu();
            JMenuItem menuItem = new JMenuItem("ลบรายการที่เลือก");
            popup.add(menuItem);
            popup.show(evt.getComponent(),evt.getX(), evt.getY());
            menuItem.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    deletetable(selltable.getSelectedRow()); 
                }
            });
        }
    }//GEN-LAST:event_selltableMouseClicked

    public void deletetable(int numrow){
        arrayid.remove(numrow);
        arraynameprod.remove(numrow);
        arraynameunit.remove(numrow);
        arraycount.remove(numrow);
        arrayrow.remove(numrow);
        arraysell.remove(numrow);
        arraysum.remove(numrow);
        showtablelist();
    }
    
    public void insertarray(){
        try {
            modellistgroup = (DefaultTableModel) selltable.getModel();
            String searchid = "SELECT * FROM product";
            ResultSet out = conn.createStatement().executeQuery(searchid);
            String textcheck = scan.getText();
            while(out.next()){
                if(textcheck.equals(out.getString("id_product"))){
                    arrayid.add(out.getString("id_product"));
                    arrayrow.add(arrayid.size());
                    arraycount.add(Integer.parseInt(count.getText()));
                    arraynameprod.add(out.getString("name_product"));
                    arraysell.add(out.getDouble("pricesell"));
                    arraynameunit.add(out.getString("unit_product"));
                    arraysum.add(out.getDouble("pricesell")*arraycount.get(arrayid.size()-1));
                    int row = arrayid.size()-1;
                    scan.setText("");
                    count.setText("1");
                    scan.requestFocus();
                    break;
                }
            }
            selltable.setModel(modellistgroup);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new cashier().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField count;
    private javax.swing.JTextField discount;
    private javax.swing.JButton exit;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JLabel name_em;
    private javax.swing.JRadioButton novat;
    private javax.swing.JLabel pricesum;
    private javax.swing.JTextField scan;
    private javax.swing.JTable selltable;
    private javax.swing.JTextField sumallprice;
    private javax.swing.JRadioButton vat;
    private javax.swing.ButtonGroup vatgroup;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
    
    public final void defalsesetpage(){
        scan.requestFocus();
        vat.setSelected(true);
    }
}
