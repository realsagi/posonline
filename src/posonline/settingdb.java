package posonline;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

public final class settingdb extends javax.swing.JFrame {
    
    BufferedReader in;
    BufferedWriter out;
    String read;

    public settingdb() {
        initComponents();
        setIcon();
        Fileread();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        server = new javax.swing.JTextField();
        user = new javax.swing.JTextField();
        pass = new javax.swing.JTextField();
        database = new javax.swing.JTextField();
        save = new javax.swing.JButton();
        cancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("ตั้งค่าการเชื่อมต่อ Database");
        setAlwaysOnTop(true);
        setBounds(new java.awt.Rectangle(100, 10, 400, 235));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setFocusTraversalPolicyProvider(true);
        setMaximizedBounds(new java.awt.Rectangle(100, 10, 400, 235));
        setMinimumSize(new java.awt.Dimension(400, 235));
        setName("settingdb"); // NOI18N
        setType(java.awt.Window.Type.POPUP);

        jLabel1.setText("Server");

        jLabel2.setText("Username");

        jLabel3.setText("Passwords");

        jLabel4.setText("Name Database");

        user.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        save.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Floppy-Small-icon.png"))); // NOI18N
        save.setToolTipText("");
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveMouseClicked(evt);
            }
        });

        cancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancel-icon.png"))); // NOI18N
        cancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(save)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancel))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(server)
                        .addComponent(user)
                        .addComponent(pass)
                        .addComponent(database, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)))
                .addContainerGap(72, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(server, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(user, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(pass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(database, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(save)
                    .addComponent(cancel))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelMouseClicked
        this.dispose();
    }//GEN-LAST:event_cancelMouseClicked

    private void saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseClicked
        Filewrite();
        this.dispose();
    }//GEN-LAST:event_saveMouseClicked
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new settingdb().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancel;
    private javax.swing.JTextField database;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField pass;
    private javax.swing.JButton save;
    private javax.swing.JTextField server;
    private javax.swing.JTextField user;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
    
    public void Fileread(){
        try{
            int linenum = 0;
            String[] data = new String[5];
            in = new BufferedReader(new FileReader("database.txt"));
            while(linenum < 4){
                read = in.readLine();
                data[linenum] = read;
                linenum++;
            }
            server.setText(data[0]);
            user.setText(data[1]);
            pass.setText(data[2]);
            database.setText(data[3]);
            in.close();
        }catch(IOException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
    public void Filewrite(){
        try {
            out = new BufferedWriter(new FileWriter("database.txt",false));
            out.write(server.getText());
            out.newLine();
            out.write(user.getText());
            out.newLine();
            out.write(pass.getText());
            out.newLine();
            out.write(database.getText());
            out.newLine();
            out.close();
            JOptionPane.showMessageDialog(this,"บันทึกข้อมูลเสร็จสิ้น","บันทึกข้อมูล",JOptionPane.PLAIN_MESSAGE);
        }catch(IOException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }

}
