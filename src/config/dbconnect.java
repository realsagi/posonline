package config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class dbconnect extends javax.swing.JFrame  {
    
    public BufferedReader in;
    public String read;
    public String[] data = new String[5];
    public Connection getconnection(){
        try{
            Fileread();
            Class.forName(com.mysql.jdbc.Driver.class.getName());
            String server = "jdbc:mysql://"
                    + data[0]
                    + "/"
                    + data[3];
            String user = data[1];
            String pass = data[2];
            return DriverManager.getConnection(server,user,pass);
        }
        catch(ClassNotFoundException | SQLException e){
           JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
    
    public void Fileread(){
        try{
            int linenum = 0;
            in = new BufferedReader(new FileReader("database.txt"));
            while(linenum < 4){
                read = in.readLine();
                data[linenum] = read;
                linenum++;
            }
            in.close();
        }catch(Exception e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
    
}
